package fr.blessedraccoon.minecraft.plugins.configmanager;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

/**
 * @author BlessedRaccoon
 *
 * <p>To handle plugin configuration, you need to extend this class.
 * Then, you'll need to add public static variable (unfortunately, they
 * can't be final with this way of doing).
 * Finally, you will link this variable with a line on the constructor.</p>
 *
 * <p>Example:</p>
 * <code>this.MY_VARIABLE = this.addAndLoadString("key", "defaultValue");</code>
 *
 * <p>If you want to proceed with a particular object there is a
 * config method for getting, you can use the generic one and passing
 * the function on argument</p>
 *
 * <p>Example:</p>
 * <code>this.MY_VARIABLE = this.addAndLoadString("key", "defaultValue", this.plugin.getConfig()::getString)</code>
 */
public abstract class Config {

    protected Plugin plugin;

    /**
     * Builds a Config object.
     * This object must be unique per plugin.
     * @param plugin plugin
     */
    public Config(@NotNull Plugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Adds a path and a default value to default config.
     * Then reads the real value on the config.yml file.
     * @param key YML key (ex: <code>map.center.x</code>)
     * @param defaultValue default value for this key (ex: <code>50</code>)
     * @param fn function to apply to load the variable from config.yml (ex: <code>this.plugin.getConfig()::getInt</code>
     * @param <T> can be anything that can be loaded up with Bukkit config functions
     * @return Loaded value from config.yml, or default value if absent
     */
    @NotNull
    public final <T> T addAndLoad(@NotNull String key, @NotNull T defaultValue, @NotNull Function<String, T> fn) {
        plugin.getConfig().addDefault(key, defaultValue);
        return fn.apply(key);
    }

    /**
     * Same as #addAndLoad, for Strings
     * @see #addAndLoad(String, Object, Function)
     * @param key key
     * @param defaultValue default value
     * @return Loaded value from config.yml, or default value if absent
     */
    @NotNull
    public final String addAndLoadString(@NotNull String key, @NotNull String defaultValue) {
        return this.addAndLoad(key, defaultValue, plugin.getConfig()::getString);
    }

    /**
     * Same as #addAndLoad, for Integers
     * @see #addAndLoad(String, Object, Function)
     * @param key key
     * @param defaultValue default value
     * @return Loaded value from config.yml, or default value if absent
     */
    public final int addAndLoadInt(@NotNull String key, int defaultValue) {
        return this.addAndLoad(key, defaultValue, plugin.getConfig()::getInt);
    }

    /**
     * Same as #addAndLoad, for Doubles
     * @see #addAndLoad(String, Object, Function)
     * @param key key
     * @param defaultValue default value
     * @return Loaded value from config.yml, or default value if absent
     */
    public final double addAndLoadDouble(@NotNull String key, double defaultValue) {
        return this.addAndLoad(key, defaultValue, plugin.getConfig()::getDouble);
    }

    /**
     * Same as #addAndLoad, for Booleans
     * @see #addAndLoad(String, Object, Function)
     * @param key key
     * @param defaultValue default value
     * @return Loaded value from config.yml, or default value if absent
     */
    public final boolean addAndLoadBoolean(@NotNull String key, boolean defaultValue) {
        return this.addAndLoad(key, defaultValue, plugin.getConfig()::getBoolean);
    }

    /**
     * Saves the default configuration to the `config.xml` file.
     */
    public final void save() {
        this.plugin.getConfig().options().copyDefaults(true);
        this.plugin.saveConfig();
    }

}
